package ru.r5design.fun.diffbreaker.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Round {

    public String aUrl;
    public String bUrl;
    public Integer level;
    public Integer coins;
    public ArrayList<String> diffs;
    public Set<Integer> openDiffs;
}
