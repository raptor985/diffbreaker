package ru.r5design.fun.diffbreaker.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Init {

    public User user;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class User {
        public Integer coins;
        public Integer level;
    }
}
