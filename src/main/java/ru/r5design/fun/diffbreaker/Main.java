package ru.r5design.fun.diffbreaker;

import java.io.IOException;

public class Main implements Runnable {

    private void solveTo(GameOperator go, Integer level, Integer coins) throws IOException {
        int maxLevel=Integer.MAX_VALUE;
        if (level!=null) {
            maxLevel=level;
        }
        int maxCoins=Integer.MAX_VALUE;
        if (coins!=null) {
            maxCoins=coins;
        }
        while (go.getLevel()<maxLevel && go.getCoins()<maxCoins) {
            go.solveRound();
        }
    }

    public void run() {
        GameOperator go = null;
        try {
            go = new GameOperator();
            solveTo(go, 210000, null);
        } catch (IOException e) {
            e.printStackTrace();
            go.close();
        }
    }

    public static void main(String[] args) throws IOException {
        Main app = new Main();
        app.run();
    }
}
