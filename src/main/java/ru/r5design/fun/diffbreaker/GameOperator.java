package ru.r5design.fun.diffbreaker;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.*;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import ru.r5design.fun.diffbreaker.json.Init;
import ru.r5design.fun.diffbreaker.json.Round;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class GameOperator {

    private final Properties properties = new Properties();
    private final ObjectMapper mapper = new ObjectMapper();
    private final GameState gs = new GameState();
    private final Logger logger = new Logger();
    private final Client client = new Client();
    private final Timer timer = new Timer();
    //private final Keeper keeper = new Keeper();

    public GameOperator() throws IOException {
        getInitParams();
        logger.logStart();
        FileInputStream fis = new FileInputStream("src/main/resources/app.properties");
        properties.load(fis);
    }

    public void close() {
        client.close();
    }

    private void getInitParams() throws IOException {
        HashMap<String, String> params = new HashMap<String, String>(){{
            put("method", "init");
            put("friends", "19083639");
        }};
        String body = client.makePostRequest(params);
        Init init = mapper.readValue(body, Init.class);
        gs.level = init.user.level;
        gs.coins = init.user.coins;
        logger.start_level = gs.level;
        logger.start_time = System.currentTimeMillis();
        timer.setOpTime();
    }

    private Round checkRound() throws IOException {
        timer.interval(500);
        HashMap<String, String> params = new HashMap<String, String>(){{
            put("method", "game");
        }};
        String body = client.makePostRequest(params);
        Round round = mapper.readValue(body,Round.class);
        timer.setOpTime();
        //keeper.savePics(round);
        gs.level = round.level;
        gs.coins = round.coins;
        return round;
    }

    private void solveDiff(final Integer num) throws IOException {
        timer.interval(100);
        HashMap<String, String> params = new HashMap<String, String>(){{
            put("method", "saveDiff");
            put("good", "1");
            put("diffId", num.toString());
        }};
        client.makeVoidPostRequest(params);
        timer.setOpTime();
    }

    public void solveRound() throws IOException {
        Round round = checkRound();
        logger.logLevel();
        logger.logSpeed();
        for (int i=1; i<=round.diffs.size(); i++) {
            if (!round.openDiffs.contains(i)) {
                solveDiff(i);
            }
        }
    }

    public int getLevel() {
        return gs.level;
    }

    public int getCoins() {
        return gs.coins;
    }

    private class Timer {
        private long lastOpTime = 0;

        void setOpTime() {
            lastOpTime = System.currentTimeMillis();
        }

        void interval(long length) {
            long passed = System.currentTimeMillis()-lastOpTime;
            if (passed>length) {
                return;
            }
            /*
            try {
                Thread.sleep(length-passed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            */
        }
    }

    private class GameState {
        Integer coins;
        Integer level;
    }

    private class Logger {

        int start_level;
        long start_time;

        void logSpeed() {
            if (gs.level%50!=0) {
                return;
            }
            double levelsPassed = gs.level-start_level;
            double timePassed = System.currentTimeMillis()-start_time;
            double speed = timePassed/levelsPassed;
            System.out.printf("Average speed: 1k. / %.4fs.\n",speed);
        }

        void logLevel() {
            System.out.println("Level: "+gs.level+", Coins: "+gs.coins);
        }

        void logStart() {
            System.out.println("Game started!");
        }
    }

    private class Keeper {

        Base64.Decoder b64Decoder = Base64.getDecoder();

        private void downloadFile(String url, String savePath) throws IOException {
            InputStream is = null;
            HttpResponse response = client.simpleGet(url);
            HttpEntity entity = response.getEntity();
            long len = entity.getContentLength();
            is = entity.getContent();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(new File(savePath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            int inByte;
            while((inByte = is.read()) != -1) {
                fos.write(inByte);
            }
            is.close();
            fos.close();
        }

        /*
        private void createLock(String path) throws IOException {
            File lockFile = new File(path);
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(lockFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            fos.write(path.getBytes());
            fos.close();
        }

        private void deleteLock(String path) {
            File lockFile = new File(path);
            lockFile.delete();
        }
        */

        void savePics(Round round) throws IOException {
            String outputDir = properties.getProperty("output_dir");
            String aFileName = round.aUrl.substring(round.aUrl.lastIndexOf('/')+1);
            File aFile = new File(outputDir+aFileName);
            if (aFile.isFile()) {
                return;
            }
            System.out.println("New image.");
            String fileNameBase = aFileName.substring(0,aFileName.indexOf('_'));
            //createLock(outputDir+fileNameBase+"_lock.txt");

            String bFileName = round.bUrl.substring(round.bUrl.lastIndexOf('/')+1);
            downloadFile(round.aUrl, outputDir+aFileName);
            downloadFile(round.bUrl, outputDir+bFileName);

            String baseDiffFilePath = outputDir+fileNameBase+"_diff_";
            int i=1;
            for (String diffB64 : round.diffs) {
                byte[] fileContents = b64Decoder.decode(diffB64);
                FileOutputStream fos = new FileOutputStream(baseDiffFilePath+(i++)+".png");
                fos.write(fileContents);
                fos.close();
            }
           // deleteLock(outputDir+fileNameBase+"_lock.txt");
        }
    }

    private class Client {
        private HashMap<String, String> headers = new HashMap<String, String>(){{
            put("Host", "galartstudio.ru");
            put("Origin", "https://vk.com");
            put("X-Requested-With", "ShockwaveFlash/22.0.0.209");
            put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
            put("Content-Type", "application/x-www-form-urlencoded");
            put("Accept-Encoding", "gzip, deflate");
        }};

        private ArrayList<NameValuePair> generalParams = new ArrayList<NameValuePair>(){{
            add(new BasicNameValuePair("platform", "vk"));
            add(new BasicNameValuePair("auth_key", "ca91f22812431e9c33e40a18dab24a86"));
            add(new BasicNameValuePair("viewer_id", "178721760"));
        }};

        private final String API_PATH = "http://galartstudio.ru/fd/api.php";
        private final CloseableHttpClient client;

        public Client() { client = HttpClients.createDefault(); }

        public HttpResponse simpleGet(String url) throws IOException {
            HttpGet request = new HttpGet(url);
            return client.execute(request);
        }

        public void close() {
            try {
                client.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        private HttpPost getBasicPost() {
            HttpPost request = new HttpPost();
            try {
                request.setURI(new URI(API_PATH));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            for (Map.Entry<String,String> entry : headers.entrySet()) {
                request.setHeader(entry.getKey(),entry.getValue());
            }
            return request;
        }

        private HttpPost getPreparedPost(Map<String,String> params) {
            HttpPost request = getBasicPost();
            ArrayList<NameValuePair> reqParams = (ArrayList<NameValuePair>) generalParams.clone();
            for (Map.Entry<String,String> entry : params.entrySet()) {
                reqParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            try {
                request.setEntity(new UrlEncodedFormEntity(reqParams));
            }
            catch (UnsupportedEncodingException e) {
                //Never
                e.printStackTrace();
            }
            return request;
        }

        public static final String UTF8_BOM = "\uFEFF";

        private String removeUTF8BOM(String s) {
            if (s.startsWith(UTF8_BOM)) {
                s = s.substring(1);
            }
            return s;
        }

        private String makePostRequest(Map<String, String> params) throws IOException {
            HttpPost request = getPreparedPost(params);
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            String body = removeUTF8BOM(EntityUtils.toString(entity,"UTF-8"));
            return body;
        }

        private void makeVoidPostRequest(Map<String, String> params) throws IOException {
            HttpPost request = getPreparedPost(params);
            HttpResponse r = client.execute(request);
            HttpEntity e = r.getEntity();
            EntityUtils.consume(e);
        }
    }
}
